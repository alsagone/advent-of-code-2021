from get_input import get_input
import sys


def get_rates(input_arr: list[str], index: int) -> tuple[str, str]:
    arr = [input_arr[i][index] for i in range(len(input_arr))]
    count_zero = 0
    count_one = 0

    for bit in arr:
        if (bit == '0'):
            count_zero += 1

        elif (bit == '1'):
            count_one += 1

        else:
            print(f"Unknown bit '{bit}'", file=sys.stderr)
            sys.exit(1)

    if (count_zero > count_one):
        gamma_rate = '0'
        epsilon_rate = '1'

    else:
        gamma_rate = '1'
        epsilon_rate = '0'

    return gamma_rate, epsilon_rate


def filter_arr(input_arr: list[str], bit: str, index: int):
    new_arr = []

    for value in input_arr:
        if (value[index] == bit):
            new_arr.append(value)

    return new_arr


def part_one(input_arr: list[str]) -> int:
    gamma_rate = ""
    epsilon_rate = ""

    for i in range(len(input_arr[0])):
        g, e = get_rates(input_arr, i)
        gamma_rate += g
        epsilon_rate += e

    return int(gamma_rate, 2) * int(epsilon_rate, 2)


def get_oxygen_rating(input_arr: list[str]) -> int:
    arr = input_arr.copy()
    index = 0

    while(len(arr) > 1 and index < len(arr[0])):
        g = get_rates(arr, index)[0]
        arr = filter_arr(arr, g, index)
        index += 1

    return int(arr[0], 2)


def get_c02_rating(input_arr: list[str]) -> int:
    arr = input_arr.copy()
    index = 0

    while (len(arr) > 1 and index < len(arr[0])):
        e = get_rates(arr, index)[1]
        arr = filter_arr(arr, e, index)
        index += 1

    return int(arr[0], 2)


def part_two(input_arr: list[str]):
    return get_oxygen_rating(input_arr) * get_c02_rating(input_arr)


if __name__ == "__main__":
    input_arr = get_input(3)
    print(f"Part one: {part_one(input_arr)}")
    print(f"Part two: {part_two(input_arr)}")
