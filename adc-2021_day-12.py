from get_input import get_input

cave_dict = {}


def parse_input(input_arr: list[str]) -> dict:
    global cave_dict
    cave_dict = {}

    for line in input_arr:
        points = line.split('-')
        start, end = points[0], points[1]

        if (start in cave_dict):
            cave_dict[start].append(end)

        else:
            cave_dict[start] = [end]

        if (end in cave_dict):
            cave_dict[end].append(start)

        else:
            cave_dict[end] = [start]

    return


def is_a_small_cave(cave_name: str) -> bool:
    return cave_name.islower()


def count_path(current_cave: str, visited_small_caves: list[str]) -> int:
    if (current_cave == "end"):
        return 1

    nb_paths = 0

    for cave in cave_dict[current_cave]:
        if not(cave in visited_small_caves):
            new_visited_small_caves = visited_small_caves.copy()

            if (is_a_small_cave(cave)):
                new_visited_small_caves.append(cave)

            nb_paths += count_path(cave, new_visited_small_caves)

    return nb_paths


def part_one():
    return count_path("start", ["start"])


def check_visited(cave: str, visited_small_caves: dict) -> bool:
    if not(is_a_small_cave(cave)) or not(cave in visited_small_caves):
        return False

    if (cave == "start" or cave == "end"):
        max_visits = 1

    else:
        max_visits = 2

    return visited_small_caves[cave] >= max_visits


def count_path_v2(current_cave: str, visited_small_caves: dict) -> int:
    if (current_cave == "end"):
        return 1

    nb_paths = 0

    for cave in cave_dict[current_cave]:
        if not(check_visited(cave, visited_small_caves)):
            if (cave in visited_small_caves):
                visited_small_caves[cave] += 1

            else:
                visited_small_caves[cave] = 1

            print(visited_small_caves)
            nb_paths += count_path_v2(cave, visited_small_caves)

    return nb_paths


def part_two():
    return count_path_v2("start", {"start": 1})


if __name__ == "__main__":
    parse_input(get_input(12))
    print(f"Part one: {part_one()}")
    print(f"Part two: {part_two()}")
