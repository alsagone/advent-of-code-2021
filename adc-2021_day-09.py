from get_input import get_input
import functools
import sys
import re

matrix = []
nb_rows = 0
nb_cols = 0


def parse_input(input_arr: list[str]) -> list[list[int]]:
    matrix = []
    pattern = r"\d"

    for line in input_arr:
        numbers = [int(x, 10) for x in re.findall(pattern, line)]
        matrix.append(numbers)

    return matrix


def is_between(x: int, a: int, b: int) -> bool:
    min_value, max_value = sorted([a, b])
    return min_value <= x and x <= max_value


def is_a_valid_position(position: tuple[int, int]) -> bool:
    global nb_rows
    global nb_cols

    row, col = position
    return is_between(row, 0, nb_rows-1) and is_between(col, 0, nb_cols-1)


def is_a_low_point(row: int, col: int) -> bool:
    global matrix
    global nb_rows
    global nb_cols

    min_adjacent_value = sys.maxsize

    #Top, Bottom, Left, Right
    for position in [(row-1, col), (row+1, col), (row, col - 1), (row, col + 1)]:
        if (is_a_valid_position(position)):
            i, j = position
            min_adjacent_value = min(
                min_adjacent_value, matrix[i][j])

    return matrix[row][col] < min_adjacent_value


def part_one() -> int:
    global matrix
    global nb_rows
    global nb_cols

    risk_level = 0

    for row in range(nb_rows):
        for col in range(nb_cols):
            if (is_a_low_point(row, col)):
                risk_level += matrix[row][col] + 1

    return risk_level


groups = []


def count_groups(row: int, col: int):
    global groups
    global matrix
    global nb_rows
    global nb_cols

    if not(is_a_valid_position((row, col))) or matrix[row][col] in [-1, 9]:
        return

    matrix[row][col] = -1
    groups[len(groups) - 1] += 1

    for position in [(row-1, col), (row+1, col), (row, col - 1), (row, col + 1)]:
        i, j = position
        count_groups(i, j)


def part_two() -> int:
    global groups
    global nb_rows
    global nb_cols

    for row in range(nb_rows):
        for col in range(nb_cols):
            groups.append(0)
            count_groups(row, col)

    biggest_groups = sorted(groups, reverse=True)[:3]
    return functools.reduce((lambda x, y: x * y), biggest_groups)


if __name__ == "__main__":
    matrix = parse_input(get_input(9))
    nb_rows = len(matrix)
    nb_cols = len(matrix[0])

    print(f"Part one: {part_one()}")
    print(f"Part two: {part_two()}")
