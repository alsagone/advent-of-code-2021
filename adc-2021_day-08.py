from get_input import get_input
import sys


def sort_string(string: str) -> str:
    return "".join(sorted(string))


def contains_string(needle: str, haystack: str):
    if (len(needle) > len(haystack)):
        return False

    b = True
    index = -1

    while (len(needle) > 0):
        try:
            index = haystack.index(needle[0])

        except ValueError:
            b = False
            break

        needle = needle[1:]
        haystack = haystack[:index] + haystack[index+1:]

    return b


def identify_numbers(segments: list[str]) -> dict:
    d = {}
    segments_to_identify = [sort_string(s) for s in segments]
    identified_segments = []

    for s in segments_to_identify:
        l = len(s)

        if (l == 2):
            d[1] = s
            identified_segments.append(s)

        elif (l == 4):
            d[4] = s
            identified_segments.append(s)

        elif (l == 3):
            d[7] = s
            identified_segments.append(s)

        elif (l == 7):
            d[8] = s
            identified_segments.append(s)

    for x in identified_segments:
        segments_to_identify.remove(x)

    six_found = False
    numbers_to_identify = [0, 2, 3, 5, 6, 9]

    while (len(numbers_to_identify) > 0):

        identified_segments = []

        for s in segments_to_identify:

            if (len(s) == 6):
                if (9 in numbers_to_identify and contains_string(d[4], s)):
                    d[9] = s
                    identified_segments.append(s)
                    numbers_to_identify.remove(9)

                elif (0 in numbers_to_identify and contains_string(d[1], s)):
                    d[0] = s
                    identified_segments.append(s)
                    numbers_to_identify.remove(0)

                elif not(six_found):
                    d[6] = s
                    six_found = True
                    identified_segments.append(s)
                    numbers_to_identify.remove(6)

                else:
                    print(f'Unable to identify "{s}')
                    sys.exit(1)

            elif (len(s) == 5):
                if (5 in numbers_to_identify and six_found and contains_string(s, d[6])):
                    d[5] = s
                    identified_segments.append(s)
                    numbers_to_identify.remove(5)

                elif (3 in numbers_to_identify and contains_string(d[1], s)):
                    d[3] = s
                    identified_segments.append(s)
                    numbers_to_identify.remove(3)

                elif (2 in numbers_to_identify and six_found):
                    d[2] = s
                    identified_segments.append(s)
                    numbers_to_identify.remove(2)

                elif (six_found):
                    print(f'Unable to identify "{s}')
                    sys.exit(1)

        for x in identified_segments:
            segments_to_identify.remove(x)

    numbers = {}

    for (n, segment) in d.items():
        numbers[segment] = n

    return numbers


def part_one(input_arr: list[str]) -> int:
    count = 0
    easy_lengths = [2, 3, 4, 7]

    for line in input_arr:
        split = line.split('|')
        digits = split[1].strip().split(" ")

        for d in digits:
            if len(d) in easy_lengths:
                count += 1

    return count


def part_two(input_arr: list[str]) -> int:
    count = 0

    for line in input_arr:

        split = line.split('|')
        segments = split[0].strip().split(" ")
        digits = split[1].strip().split(" ")

        numbers = identify_numbers(segments)
        number_str = ""

        for d in [sort_string(x) for x in digits]:
            number_str += str(numbers[d])

        count += int(number_str, 10)

    return count


if __name__ == "__main__":
    input_arr = get_input(8)
    print(f"Part one: {part_one(input_arr)}")
    print(f"Part two: {part_two(input_arr)}")
