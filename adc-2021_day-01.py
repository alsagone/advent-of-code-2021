from get_input import get_input


def part_one(input_arr: list[int]) -> int:
    count = 0

    for i in range(len(input_arr) - 1):
        if (input_arr[i] < input_arr[i+1]):
            count += 1

    return count


def build_sliding_windows(input_arr: list[int], offset: int) -> list[list[int]]:
    return [input_arr[i:i+offset] for i in range(len(input_arr))]


def part_two(input_arr: list[int]) -> None:
    sum_sliding_windows = [sum(s) for s in build_sliding_windows(input_arr, 3)]
    return part_one(sum_sliding_windows)


if __name__ == "__main__":
    input_arr = [int(x) for x in get_input(1)]
    print(f"Part one: {part_one(input_arr)}")
    print(f"Part two: {part_two(input_arr)}")
