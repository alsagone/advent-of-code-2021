from get_input import get_input
import collections


def evolve(lanternfishs: list[int], nb_days: int) -> int:
    current_stage = collections.Counter(lanternfishs)

    for i in range(0, 9):
        if not(i in current_stage):
            current_stage[i] = 0

    for _ in range(nb_days):
        new_lanternfishs = current_stage[0]

        for i in range(8):
            current_stage[i] = current_stage[i+1]

        current_stage[6] += new_lanternfishs
        current_stage[8] = new_lanternfishs

    return sum(current_stage.values())


def part_one(input_arr: list[int]) -> int:
    return evolve(input_arr, 80)


def part_two(input_arr: list[int]) -> int:
    return evolve(input_arr, 256)


if __name__ == "__main__":
    input_arr = [int(n) for n in get_input(6)[0].split(",")]
    print(f"Part one: {part_one(input_arr)}")
    print(f"Part two: {part_two(input_arr)}")
