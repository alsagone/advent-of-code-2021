import collections
from get_input import get_input


closing_chars = [']', ')', '}', '>']

matching_char = {'[': ']', ']': '[', '(': ')', ')': '(',
         '{': '}', '}': '{', '<': '>', '>': '<'}


def find_incorrect_char(string: str) -> str:
    incorrect_char = None
    stack = []

    for char in string:
        if (char in closing_chars):
            if (len(stack) == 0 or char != matching_char[stack.pop()]):
                incorrect_char = char
                break

        else:
            stack.append(char)

    return incorrect_char


def part_one(input_arr: list[str]) -> int:
    corrupted_chars = []
    total_score = 0
    score_dict = {')': 3, ']': 57, '}': 1197, '>': 25137}

    for line in input_arr:
        incorrect_char = find_incorrect_char(line)

        if not(incorrect_char is None):
            corrupted_chars.append(incorrect_char)

    count = collections.Counter(corrupted_chars)

    for (char, occurences) in count.items():
        total_score += occurences * score_dict[char]

    return total_score


def is_missing(string: str, index: int) -> bool:
    char = string[index]

    if string[index] in closing_chars:
        return False

    closing_char = matching_char[char]
    score = 1
    b = True

    for i in range(index+1, len(string), 1):
        current_char = string[i]

        if current_char == char:
            score += 1

        elif current_char == closing_char:
            score -= 1

            if score == 0:
                b = False
                break

    return b


def get_missing_chars(string: str) -> dict:
    missing_chars = ""

    for i in range(len(string)):
        if (is_missing(string, i)):
            missing_chars = matching_char[string[i]] + missing_chars

    return missing_chars


def is_incomplete(line: str) -> bool:
    return find_incorrect_char(line) is None


def part_two(input_arr: list[str]) -> int:
    scores_arr = []
    score_dict = {')': 1, ']': 2, '}': 3, '>': 4}

    incomplete_lines = [line for line in input_arr if is_incomplete(line)]

    for line in incomplete_lines:
        completion_score = 0
        missing_chars = get_missing_chars(line)

        for char in missing_chars:
            completion_score = (5 * completion_score) + score_dict[char]

        scores_arr.append(completion_score)

    middle_index = (len(scores_arr) - 1)//2
    return sorted(scores_arr)[middle_index]


if __name__ == "__main__":
    input_arr = get_input(10)
    print(f'Part one: {part_one(input_arr)}')
    print(f'Part two: {part_two(input_arr)}')
