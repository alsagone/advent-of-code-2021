from get_input import get_input
import sys


def calculate_fuel_cost(starting_position: int, positions: list[int]) -> int:
    total_fuel = 0

    for n in positions:
        total_fuel += abs(n - starting_position)

    return total_fuel


def sum_n(n: int) -> int:
    return n * (n+1) // 2


def calculate_fuel_cost_v2(starting_position: int, positions: list[int]):
    total_fuel = 0

    for n in positions:
        total_fuel += sum_n(abs(n - starting_position))

    return total_fuel


def part_one(input_arr: list[int]) -> int:
    min_fuel = sys.maxsize
    
    sorted_arr = sorted(input_arr)
    min_position, max_position = sorted_arr[0], sorted_arr[-1]

    for position in range(min_position, max_position+1, 1):
        min_fuel = min(min_fuel, calculate_fuel_cost(position, input_arr))

    return min_fuel


def part_two(input_arr: list[int]) -> int:
    min_fuel = sys.maxsize

    sorted_arr = sorted(input_arr)
    min_position, max_position = sorted_arr[0], sorted_arr[-1]
    
    for position in range(min_position, max_position+1, 1):
        min_fuel = min(min_fuel, calculate_fuel_cost_v2(position, input_arr))

    return min_fuel


if __name__ == "__main__":
    input_arr = [int(x) for x in get_input(7)[0].split(",")]
    print(f"Part one: {part_one(input_arr)}")
    print(f"Part two: {part_two(input_arr)}")
