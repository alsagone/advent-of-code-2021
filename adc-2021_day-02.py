from get_input import get_input
import re
import sys


def get_instruction_value(instruction: str) -> int:
    match = re.search(r"(\d+)", instruction)

    if match:
        return int(match.group(1))

    else:
        print(f"Unknown instruction: '{instruction}'")
        sys.exit(1)


def part_one(input_arr: list[str]) -> int:
    horizontal = 0
    depth = 0

    for instruction in input_arr:
        value = get_instruction_value(instruction)

        if ("forward" in instruction):
            horizontal += value

        elif ("down" in instruction):
            depth += value

        elif ("up" in instruction):
            depth -= value

        else:
            print(f"Unknown instruction: '{instruction}'")
            sys.exit(1)

    return horizontal * depth


def part_two(input_arr: list[str]) -> int:
    horizontal = 0
    depth = 0
    aim = 0

    for instruction in input_arr:
        value = get_instruction_value(instruction)

        if ("forward" in instruction):
            horizontal += value
            depth += aim * value

        elif ("down" in instruction):
            aim += value

        elif ("up" in instruction):
            aim -= value

        else:
            print(f"Unknown instruction: '{instruction}'")
            sys.exit(1)

    return horizontal * depth


if __name__ == "__main__":
    input_arr = get_input(2)
    print(f"Part one: {part_one(input_arr)}")
    print(f"Part two: {part_two(input_arr)}")
