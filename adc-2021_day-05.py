from typing import Collection
import numpy as np
from get_input import get_input
import re
import collections


def parse_input(input_array: list[str]) -> list[tuple[int, int]]:
    arr = []

    for line in input_array:
        numbers = [int(x) for x in re.findall(r"(\d+)", line)]

        if (len(numbers) == 4):
            arr.append([(numbers[0], numbers[1]), (numbers[2], numbers[3])])

        else:
            print(numbers)
            raise ValueError

    return arr


def is_horizontal_or_vertical(point_A: tuple[int, int], point_B: tuple[int, int]) -> bool:
    return (point_A[0] == point_B[0]) or (point_A[1] == point_B[1])


def get_nb_points_between(point_A: tuple[int, int], point_B: tuple[int, int]) -> list[tuple[int, int]]:
    points_list = []

    if (point_A[0] == point_B[0]):
        x = point_A[0]
        start, end = sorted([point_A[1], point_B[1]])

        for i in range(start, end+1):
            points_list.append((x, i))

    elif (point_A[1] == point_B[1]):
        y = point_A[1]
        start, end = sorted([point_A[0], point_B[0]])

        for i in range(start, end+1):
            points_list.append((i, y))

    else:
        x_increment = 1 if point_A[0] < point_B[0] else -1
        y_increment = 1 if point_A[1] < point_B[1] else -1

        x, y = point_A

        while (x != point_B[0] and y != point_B[1]):
            points_list.append((x, y))
            x += x_increment
            y += y_increment

        points_list.append(point_B)

    return points_list


def part_one(input_arr: list[tuple[int, int]]):
    points_list = []
    for arr in input_arr:
        if is_horizontal_or_vertical(arr[0], arr[1]):
            for point in get_nb_points_between(arr[0], arr[1]):
                points_list.append(point)

    count = collections.Counter(points_list)

    intersections = 0

    for value in count.values():
        if (value > 1):
            intersections += 1

    return intersections


def part_two(input_arr: list[tuple[int, int]]):
    points_list = []
    for arr in input_arr:
        for point in get_nb_points_between(arr[0], arr[1]):
            points_list.append(point)

    count = collections.Counter(points_list)

    intersections = 0

    for value in count.values():
        if (value > 1):
            intersections += 1

    return intersections


if __name__ == "__main__":
    input_arr = parse_input(get_input(5))
    print(f"Part one: {part_one(input_arr)}")
    print(f"Part two: {part_two(input_arr)}")
