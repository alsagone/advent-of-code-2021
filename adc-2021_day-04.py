from get_input import get_input
import re
import sys


def parse_line(line: str) -> list[int]:
    return [int(x) for x in re.findall(r"(\d+)", line)]


def get_grid(input_arr: list[str]) -> dict:
    grid = {}
    i = 0

    if (len(input_arr[0]) > 0):
        print("Error: input_arr must start with an empty string")
        sys.exit(1)

    for line in input_arr:
        if (len(line) > 0):
            grid[i].append(parse_line(line))

        else:
            i += 1
            grid[i] = []

    return grid


def remove_number(arr: list[list[int]], n: int) -> list[list[int]]:
    temp = []

    for row in arr:
        t = []

        for x in row:
            if x != n:
                t.append(x)

            else:
                t.append(-1)

        temp.append(t)

    return temp


def draw_number(grid: dict, n: int) -> dict:
    for (key, value) in grid.items():
        grid[key] = remove_number(value, n)

    return grid


def check_win(grid: list[list[int]]) -> bool:
    # Rows
    for row in grid:
        if all(x == -1 for x in row):
            return True

    # Cols

    for i in range(len(grid)):
        col = [row[i] for row in grid]

        if all(x == -1 for x in col):
            return True

    return False


def grid_sum(grid: list[list[int]]):
    total = 0
    for row in grid:
        for x in row:
            if x != -1:
                total += x

    return total


def part_one(input_arr: list[str]) -> int:
    draw = parse_line(input_arr[0])
    grid = get_grid(input_arr[1:])
    winning_grid = -1

    while (len(draw) > 0 and winning_grid == -1):
        number = draw.pop(0)
        grid = draw_number(grid, number)

        for i in grid.keys():
            if check_win(grid[i]):
                winning_grid = i

    if (winning_grid != -1):
        return grid_sum(grid[winning_grid]) * number

    else:
        raise ValueError


def part_two(input_arr: list[str]) -> int:
    draw = parse_line(input_arr[0])
    grid = get_grid(input_arr[1:])
    grid_numbers = list(grid.keys())
    result = -1

    while (len(draw) > 0 and result == -1):
        number = draw.pop(0)

        grid = draw_number(grid, number)

        for i in grid_numbers:
            if check_win(grid[i]):

                if (len(grid_numbers) > 1):
                    grid_numbers.remove(i)

                else:
                    result = grid_sum(grid[i]) * number

    if (result == -1):
        raise ValueError

    return result


if __name__ == "__main__":
    input_arr = get_input(4)
    print(f"Part one: {part_one(input_arr)}")
    print(f"Part two: {part_two(input_arr)}")
