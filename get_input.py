import platform


def get_input(day: int) -> list:
    separator = '\\' if platform.system() == 'Windows' else '/'

    if day != 0:
        input_file = "inputs" + separator + f"input-{day:02d}.txt"

    else:
        input_file = "inputs" + separator + "test.txt"

    with open(input_file) as f:
        input_arr = f.read().splitlines()

    return input_arr
