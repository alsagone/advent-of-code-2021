from get_input import get_input
import numpy as np
import re

matrix: np.ndarray = []
flash_matrix: np.ndarray = []
nb_rows: int = 0
nb_cols: int = 0


def parse_input(input_arr: list[str]):
    arr = []
    pattern = r"\d"

    for line in input_arr:
        numbers = [int(x, 10) for x in re.findall(pattern, line)]
        arr.append(numbers)

    return np.array(arr)


def init_matrix():
    global matrix
    global nb_rows
    global nb_cols

    matrix = parse_input(get_input(11))
    nb_rows, nb_cols = matrix.shape
    return


def is_between(x: int, a: int, b: int) -> bool:
    min_value, max_value = sorted([a, b])
    return min_value <= x and x <= max_value


def is_a_valid_position(position: tuple[int, int]) -> bool:
    global nb_rows
    global nb_cols

    row, col = position
    return is_between(row, 0, nb_rows-1) and is_between(col, 0, nb_cols-1)


def increase_energy_level():
    global matrix
    global nb_rows
    global nb_cols

    for row in range(nb_rows):
        for col in range(nb_cols):
            matrix[row][col] += 1

    return


def get_neighbours(row: int, col: int) -> list[tuple[int, int]]:
    neighbours = []

    for i in range(row - 1, row + 2, 1):
        for j in range(col - 1, col + 2, 1):
            if (i == row and j == col):
                pass

            position = (i, j)
            if is_a_valid_position(position):
                neighbours.append(position)

    return neighbours


def flash(row: int, col: int):
    global matrix
    global flash_matrix

    if not(flash_matrix[row][col]):
        flash_matrix[row][col] = True

        for position in get_neighbours(row, col):
            x, y = position
            matrix[x][y] += 1

            if (matrix[x][y] > 9):
                flash(x, y)

    return


def reset_energy_level():
    global matrix
    global nb_rows
    global nb_cols

    for row in range(nb_rows):
        for col in range(nb_cols):
            if (matrix[row][col] > 9):
                matrix[row][col] = 0

    return


def perform_cycle():
    global matrix
    global flash_matrix
    global nb_rows
    global nb_cols

    flash_matrix = np.tile(False, (nb_rows, nb_cols))
    increase_energy_level()

    for row in range(nb_rows):
        for col in range(nb_cols):
            if (matrix[row][col] > 9):
                flash(row, col)

    reset_energy_level()
    return


def count_flashes():
    global flash_matrix

    perform_cycle()
    return np.count_nonzero(flash_matrix == True)


def part_one():
    init_matrix()
    flashes_count = 0

    for _ in range(100):
        flashes_count += count_flashes()

    return flashes_count


def part_two():
    global flash_matrix

    init_matrix()
    cycles_count = 0
    all_flashed_simultaneously = False

    while not(all_flashed_simultaneously):
        cycles_count += 1
        perform_cycle()
        all_flashed_simultaneously = np.all(flash_matrix == True)

    return cycles_count


if __name__ == "__main__":
    print(f"Part one: {part_one()}")
    print(f"Part two: {part_two()}")
